***TABLE CREATION***

CREATE TABLE Job (
JobID NUMBER(6) NOT NULL,
JobTitle VARCHAR2(20),
HourlyRate NUMBER(4),
Bonus NUMBER(4),

CONSTRAINT pk_Job_JobID PRIMARY KEY (JobID))

CREATE TABLE Event (
EventID NUMBER(6) NOT NULL,
SignUpDate DATE,
Event DATE,
EventLocation VARCHAR2(20),
NumberOfGuests NUMBER(38),
CateringType VARCHAR2(20),
Status VARCHAR2(20),
Price NUMBER(5),

CONSTRAINT pk_Event_EventID PRIMARY KEY (EventID))

CREATE TABLE Client (
ClientID NUMBER(6) NOT NULL,
Title VARCHAR2(20),
FirstName VARCHAR2(20),
LastName VARCHAR2(20),
Address VARCHAR2(50),
City VARCHAR2(20),
PostCode CHAR(7),
Area VARCHAR2(20),
PhoneNo NUMBER(11),
Feedback VARCHAR2(99),
Discount VARCHAR2(20),

CONSTRAINT pk_Client_ClientID PRIMARY KEY (ClientID))

CREATE TABLE Staff (
StaffID NUMBER(6) NOT NULL,
Title VARCHAR2(20),
FirstName VARCHAR2(20),
LastName VARCHAR2(20),
Address VARCHAR2(50),
City VARCHAR2(20),
PostCode CHAR(7),
Area VARCHAR2(20),
PhoneNo NUMBER(11),
JobID NUMBER(6) NOT NULL,
HrsWorked NUMBER(4),
Status VARCHAR2(20),

CONSTRAINT pk_Staff_StaffID PRIMARY KEY (StaffID),
CONSTRAINT fk_Staff_JobID FOREIGN KEY (JobID)
REFERENCES Job(JobID))

CREATE TABLE RequirementList (
EventID NUMBER(6) NOT NULL,
KitchenStaffNo NUMBER(38),
FloorStaffNo NUMBER (38),

CONSTRAINT fk_RequirementList_EventID FOREIGN KEY (EventID)
REFERENCES Event(EventID))

CREATE TABLE Schedule (
StaffID NUMBER(6) NOT NULL,
EventID NUMBER(6) NOT NULL,
HrsWorked NUMBER(2),

CONSTRAINT fk_Schedule_StaffID FOREIGN KEY (StaffID)
REFERENCES Staff(StaffID),

CONSTRAINT fk_Schedule_EventID FOREIGN KEY (EventID)
REFERENCES Event(EventID))

CREATE TABLE Feedback (
StaffID NUMBER(6) NOT NULL,
ClientID NUMBER(6) NOT NULL,
Comments VARCHAR2(99),

CONSTRAINT fk_Feedback_StaffID FOREIGN KEY (StaffID)
REFERENCES Staff(StaffID),

CONSTRAINT fk_Feedback_ClientID FOREIGN KEY (ClientID)
REFERENCES Client(ClientID))



***DATA ENTRIES***

INSERT INTO Staff (StaffID, Title, FirstName, LastName, Address, City, PostCode, Area, PhoneNo, JobID, Status)
VALUES 
('342123', 'MR', 'JOHN', 'SMITH', '33 JONES ROAD', 'LONDON', 'YEH 7RE', 'WEST', '07563749286', '285327', 'WORKING'),
('437526', 'MRS', 'KATE', 'HOBBS', '43 HILL ROAD', 'LONDON', 'GFD 8IU', 'NORTH', '07793640214', '098243', 'WORKING'),
('356198', 'MR', 'PETER', 'DESK', '77 YALE HILL', 'LONDON', 'TT5 HJ8', 'NORTH', '07839462574', '935720', 'N/A'),
('673144', 'MRS', 'JANE', 'REDDING', '7 GALE ROAD', 'LONDON', '4GH YUU', 'NORTH', '07534720667', '784224', 'WORKING'),
('345433', 'MRS', 'SARAH', 'JONES', '65 DENNY AVENUE', 'LONDON', 'JHG 8UI', 'SOUTH', '07835478399', '652943', 'N/A');

INSERT INTO Job (JobID, JobTitle, HourlyRate, Bonus)
VALUES 
('342123', 'CHEFF', '8.00', '0'),
('543736', 'WAITER', '5.00', '0'),
('768769', 'WAITRESS', '5.00', '25.00'),
('769477', 'WAITRESS', '5.00', '50.00'),
('743811', 'MANGER', '11.00', '0');

INSERT INTO Event (EventID, SignUpDate, Event, EventLocation, NumberOfGuests, CateringType, Status, Price)
VALUES 
('312756', '3-JAN-13', '6-JUN-13', 'MAIN-HALL', '20', 'BUFFET', 'FULL', '90'),
('658434', '9-APR-12', '8-APR-14', 'MAIN-HALL', '50', 'BUFFET', 'FULL', '600'),
('724859', '11-MAY-13', '13-DEC-13', 'PRIVATE-SUIT', '10', 'SET-MEAL', 'FULL', '500'),
('547277', '20-FEB-13', '23-NOV-13', 'MAIN-HALL', '20', 'ALL-YOU-CAN-EAT', 'NOT FULL', '250'),
('479775', '6-JUL-11', '15-DEC-13', 'PRIVATE-SUITE', '2', 'SET-MEAL', 'FULL', '200');


INSERT INTO RequirementList (EventID, KitchenStaffNo, FloorStaffNo)
VALUES 
('312756', '3', '8'),
('658434', '8', '25'),
('724859', '3', '3'),
('547277', '5', '4'),
('479775', '1','1');

INSERT INTO Schedule (StaffID, EventID, HrsWorked)
VALUES
('863256', '312756', '55'),
('356198', '685434', '60'),
('437526', '724859', '35'),
('673144', '547277', '40'),
('345433', '479775', '20');

INSERT INTO Client (ClientID, Title, FirstName, LastName, Address, City, PostCode, Area, PhoneNo, Feedback, Discount)
VALUES
('574355', 'MR', 'DAVE', 'MONSOON', '22 KNIGHT CLOSE', 'LONDON', 'UFJ 7TT', 'NORTH', '02074857294', 'GOOD', 'NO'),
('946777', 'MRS', 'LUCY', 'HUDSON', '62 RENLY HILL', 'LONDON', 'SE4 TR5', 'SOUTH', '07736495027', 'GOOD', 'YES'),
('134788', 'MRS', 'HELEN', 'BROWN', '1 EVERGREEN CLOSE', 'LONDON', 'KL8 OP0', 'NORTH', '07632778112', 'BAD', 'NO'),
('677933', 'MR', 'SAM', 'SOLOMUN', '15 WESTER AVENUE', 'LONDON', 'AS4 LI9', 'SOUTH', '07830482754', 'BAD', 'YES'),
('452666', 'MRS', 'SALLY', 'HUNT', '66 BLEMINGTON ROAD', 'LONDON', 'DS6 KH8', 'SOUTH', '07822542119', 'GOOD', 'YES');

INSERT INTO Feedback (StaffID, ClientID, Comments)
VALUES
('863256', '574355', 'VERY QUICK SERVICES AND POLITE, WOULD USE AGAIN!'),
('356198', '946777', 'FOOD WAS GREAT AND SERVICES WERE EXCELLENT!'),
('437526', '134788', 'HAD TO WAIT NEARLY 30 MINS TO ORDER!'),
('673144', '677933', 'THE FOOD DELIVERD WAS NOT WHAT I ORDERD AND WASTED MY TIME!'),
('345433', '452666', 'LOVELY FOOD AND THE WAITERS WERE QUICK TO TAKE ORDER!');



***VIEWS***

CREATE VIEW PaySlip AS
SELECT StaffID, Job.JobID, Title, FirstName, LastName, Address, City, PostCode, Area, PhoneNo, JobTitle, HourlyRate, Bonus
FROM Staff, Job;



***Queries***

SELECT *
FROM STAFF
WHERE STATUS = 'WORKING';

SELECT *
FROM Client
WHERE Discount = 'YES'
ORDER BY LastName;

SELECT DISTINCT StaffID, Client.ClientID, Feedback, Comments
FROM Client, Feedback
WHERE Feedback = 'GOOD';

SELECT DISTINCT EventID, EventDate, EventLocation, CateringType
FROM Event
WHERE EventDate LIKE '%JUN%';

SELECT ClientID, Title, FirstName, LastName, Address, City, PostCode, Area, PhoneNo, Event.EventDate
FROM Client, Event
WHERE EventDate LIKE '%';

SELECT Job.JobID, JobTitle, Status, FirstName, LastName
FROM Job, Staff
WHERE Status = 'WORKING';

SELECT StaffID, Title, FirstName, LastName, Address, City, PostCode, Area, PhoneNo
FROM Staff
ORDER BY LastName;

SELECT ClientID, Title, FirstName, LastName, Address, City, PostCode, Area, PhoneNo
FROM Client
ORDER BY LastName;